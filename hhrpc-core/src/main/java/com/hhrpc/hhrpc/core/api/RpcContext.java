package com.hhrpc.hhrpc.core.api;

import com.google.common.base.MoreObjects;
import com.google.common.collect.Maps;
import com.hhrpc.hhrpc.core.consumer.HttpInvoker;
import com.hhrpc.hhrpc.core.meta.InstanceMeta;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Date 2024/3/22
 * @Author lifei
 */
public class RpcContext {
    private List<Filter> filterList;
    private Router<InstanceMeta> router;
    private LoadBalance<InstanceMeta> loadBalance;
    private Map<String, String> parameters = new HashMap<>();

    private HttpInvoker httpInvoker;

    public static ThreadLocal<Map<String, String>> contextParameters = new ThreadLocal<>(){
        @Override
        protected Map<String, String> initialValue() {
            return Maps.newHashMap();
        }
    };

    public static void putContextParameter(String key, String value) {
        contextParameters.get().put(key, value);
    }

    public static String getContextParameter(String key) {
        return contextParameters.get().get(key);
    }

    public RpcContext() {}

    public RpcContext(Router<InstanceMeta> router, LoadBalance<InstanceMeta> loadBalance, HttpInvoker httpInvoker) {
        this.router = router;
        this.loadBalance = loadBalance;
        this.httpInvoker = httpInvoker;
    }

    public String param(String key) {
        return parameters.get(key);
    }

    public List<Filter> getFilterList() {
        return filterList;
    }

    public void setFilterList(List<Filter> filterList) {
        this.filterList = filterList;
    }

    public Router<InstanceMeta> getRouter() {
        return router;
    }

    public void setRouter(Router<InstanceMeta> router) {
        this.router = router;
    }

    public LoadBalance<InstanceMeta> getLoadBalance() {
        return loadBalance;
    }

    public void setLoadBalance(LoadBalance<InstanceMeta> loadBalance) {
        this.loadBalance = loadBalance;
    }

    public HttpInvoker getHttpInvoker() {
        return httpInvoker;
    }

    public void setHttpInvoker(HttpInvoker httpInvoker) {
        this.httpInvoker = httpInvoker;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(RpcContext.class)
                .add("filterList", filterList)
                .add("router", router)
                .add("loadBalance", loadBalance)
                .add("httpInvoker", httpInvoker)
                .add("parameters", parameters)
                .toString();
    }
}
